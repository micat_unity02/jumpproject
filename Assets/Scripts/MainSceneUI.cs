using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainSceneUI : MonoBehaviour
{
	/// <summary>
	/// 게임 시작 버튼
	/// </summary>
	public Button m_GameStartButton;

	/// <summary>
	/// 최고 점수 텍스트
	/// </summary>
	public TMP_Text m_BestScoreText;

	private void Awake()
	{
		// 버튼 이벤트 설정
		m_GameStartButton.onClick.AddListener(OnGameStartButtonClicked);
	}

	private void Start()
	{
		m_BestScoreText.text =
			"최고 점수 : " + 
			GameManager.getInstance.m_BestScoreData.m_Score.ToString();
	}

	private void OnGameStartButtonClicked()
	{
		GameManager.getInstance.ResetScoreData();

		// GameScene 을 로드합니다.
		SceneManager.LoadScene("GameScene");
	}


}
