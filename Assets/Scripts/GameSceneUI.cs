using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameSceneUI : MonoBehaviour
{
	/// <summary>
	/// 터치하여 게임 시작 텍스트입니다.
	/// </summary>
	public TMP_Text m_TouchToPlayText;

	/// <summary>
	/// 점수를 표시하는 텍스트입니다.
	/// </summary>
	public TMP_Text m_ScoreText;


	/// <summary>
	/// 메인 씬으로 이동 버튼입니다.
	/// </summary>
	public Button m_GoToMainSceneButton;

	private void Awake()
	{
		// 버튼 이벤트 설정
		m_GoToMainSceneButton.onClick.AddListener(OnGoToMainSceneButtonClicked);

		// 버튼 비활성화
		m_GoToMainSceneButton.gameObject.SetActive(false);
	}

	private void OnGoToMainSceneButtonClicked()
	{
		Time.timeScale = 1.0f;

		// 최고 점수 갱신
		GameManager.getInstance.UpdateBestScore();

		SceneManager.LoadScene("MainScene");
	}

	public void OnGameStart()
	{
		// 텍스트 비활성화
		m_TouchToPlayText.gameObject.SetActive(false);
	}

	public void OnGameOver()
	{
		// 버튼 활성화
		m_GoToMainSceneButton.gameObject.SetActive(true);
	}

	public void UpdateScore()
	{
		m_ScoreText.text =
			GameManager.getInstance.m_ScoreData.m_Score.ToString();
	}
}
